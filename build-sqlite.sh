#!/bin/sh

cd third_party/sqlite

./configure --enable-all

make sqlite3.c

cd ext/wasm

cat >sqlite3_wasm_extra_init.c <<'EOF'
#include "sqlite3.h"
#include "../misc/spellfix.c"

int sqlite3_wasm_extra_init(const char *z){
  int nErr = 0;
  nErr += sqlite3_auto_extension((void(*)())sqlite3_spellfix_init);
  return nErr ? SQLITE_ERROR : SQLITE_OK;
}
EOF

make dist