import './app.css'
import App from './App.svelte'
import sqlite3InitModule from "../third_party/sqlite/ext/wasm/jswasm/sqlite3-bundler-friendly.mjs";

sqlite3InitModule({
  print: console.log,
  printErr: console.error,
}).then(sqlite3 => {
  window.sqlite3 = sqlite3;
  window.database = new sqlite3.oo1.DB();
});

const app = new App({
  target: document.getElementById('app'),
})

export default app
