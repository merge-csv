import sqlite3InitModule from "../third_party/sqlite/ext/wasm/jswasm/sqlite3-bundler-friendly.mjs";
import papaparse from "papaparse";

let database;
let insert;
let suspect;
let drop;
let fuzzy = [];
let export_data;
let export_suspects;
let import_threads = new Map();

const handlers = {
    // Initialize SQLite, open an in-memory database, and create a table which matches
    // the user's defined schema.
    init: async ({ schema }) => {
        const sqlite3 = await sqlite3InitModule({
            print: console.log,
            printErr: console.error,
        });

        database = new sqlite3.oo1.DB();

        sqlite3.capi.sqlite3_progress_handler(
            database,
            100000,
            () => postMessage({ tag: "heartbeat" }),
            null,
        );

        // Build an SQL statement for creating a table.
        const table_sql = ["CREATE TABLE Data ("];
        for (let idx = 0; idx < schema.length; idx++) {
            const column = schema[idx];
            const escaped = column.name.replace("\"", "\"\"");
            table_sql.push(`\n\t"${escaped}" TEXT`);
            if (column.required)
                table_sql.push("\n\t\tNOT NULL");
            if (column.unique)
                table_sql.push("\n\t\tUNIQUE");
            if (idx < schema.length - 1)
                table_sql.push(",");
        }
        table_sql.push("\n);");
        database.exec(table_sql);

        // Build a spellfix index for the fuzzy columns.
        for (const column of schema) {
            const escaped = column.name.replace("\"", "\"\"");
            database.exec(`
                CREATE VIRTUAL TABLE "Spell_${escaped}" USING spellfix1;
                CREATE TRIGGER "SpellTriggerInsert_${escaped}"
                AFTER INSERT ON Data
                BEGIN
                    INSERT INTO "Spell_${escaped}"(rowid, word)
                    VALUES (new.rowid, new."${escaped}");
                END;
                CREATE TRIGGER "SpellTriggerDelete_${escaped}"
                AFTER DELETE ON Data
                BEGIN
                    DELETE FROM "Spell_${escaped}"
                    WHERE "Spell_${escaped}".rowid = old.rowid;
                END;
                CREATE TRIGGER "SpellTriggerUpdate_${escaped}"
                AFTER UPDATE OF "${escaped}" ON Data
                BEGIN
                    UPDATE "Spell_${escaped}"
                    SET word = new."${escaped}"
                    WHERE "Spell_${escaped}".rowid = new.rowid;
                END;
            `);
        }

        // Build an SQL statement for inserting new data into the table.
        const insert_sql = ["INSERT INTO Data\nVALUES ("];
        for (let idx = 0; idx < schema.length; idx++) {
            const column = schema[idx];
            let binding = "?";
            if (!column.case_sensitive)
                binding = `lower(${binding})`;
            if (column.trim)
                binding = `trim(${binding})`;
            insert_sql.push(binding);
            if (idx < schema.length - 1)
                insert_sql.push(",");
        }
        insert_sql.push(")\nON CONFLICT DO NOTHING;");
        insert = database.prepare(insert_sql);

        database.exec("CREATE TABLE Suspect(id INTEGER PRIMARY KEY)");

        suspect = database.prepare([
            "INSERT INTO Suspect(id)\n",
            "VALUES (?)\n",
            "ON CONFLICT DO NOTHING;",
        ]);

        drop = database.prepare("DELETE FROM Data WHERE rowid = ?;");

        for (const fuzzy_column of schema) {
            if (!fuzzy_column.fuzzy)
                continue;
            const fuzzy_escaped = fuzzy_column.name.replace("\"", "\"\"");
            const sql = [];
            sql.push(
                "SELECT",
                "\n\tSpell.rowid AS id,",
                "\n\tjson_group_array(json_object(",
                "\n\t\t'id', Data.rowid,",
                "\n\t\t'distance', Spell.distance,",
                "\n\t\t'data', json_array(",
            );
            for (let idx = 0; idx < schema.length; idx++) {
                const column = schema[idx];
                const escaped = column.name.replace("\"", "\"\"");
                sql.push(`\n\t\t\tData."${escaped}"`);
                if (idx < schema.length - 1)
                    sql.push(",");
            }
            sql.push(
                "\n\t\t)",
                "\n\t)) AS cluster",
                `\nFROM "Spell_${fuzzy_escaped}" AS Spell`,
                `\nINNER JOIN Data`,
                "\nON",
                "\n\tSpell.rowid <= Data.rowid",
                `\n\tAND Spell.word MATCH Data."${fuzzy_escaped}"`,
                // TODO: Make this configurable.
                "\n\tAND Spell.distance BETWEEN 0 AND 100",
                "\nWHERE (?1 IS NULL OR Spell.rowid > ?1)",
                "\nGROUP BY Spell.rowid",
                "\nHAVING count(Data.rowid) > 1",
                "\nORDER BY Spell.rowid ASC, Spell.distance ASC",
                "\nLIMIT 1;",
            );
            const stmt = database.prepare(sql);
            fuzzy.push(stmt);
        }

        export_data = database.prepare([
            "SELECT Data.*\n",
            "FROM Data\n",
            "LEFT OUTER JOIN Suspect\n",
            "ON Data.rowid = Suspect.id;",
        ]);

        export_suspects = database.prepare([
            "SELECT Data.*\n",
            "FROM Data\n",
            "INNER JOIN Suspect\n",
            "ON Data.rowid = Suspect.id;",
        ]);

        postMessage({ tag: "ready" });
    },
    // Release all allocated resources gracefully.
    deinit: () => {
        for (const stmt of fuzzy) {
            stmt.finalize();
        }
        drop?.finalize();
        suspect?.finalize();
        insert?.finalize();
        database?.close();
        close();
    },
    load: async ({ seqid, file, preview }) => {
        // The headers extracted from the first row of the file.
        let headers = null;
        // The mapping between input headers and output headers. The index is the
        // input header and the value is the index of the output header to which it
        // should be associated.
        let associations = null;
        // An array of rows which have not been processed yet. This is done in order
        // to allow the user to interactively associate the input headers with the
        // output headers.
        let buffered = [];

        const thread = {};

        import_threads.set(seqid, thread);

        // Enclose the above state for processing the rows in multiple different
        // places below.
        const process_rows = (rows) => {
            for (const row of rows) {
                // Bind the input row while remapping the column order and/or
                // dropping unused columns.
                for (let idx = 0; idx < row.length; idx++) {
                    const output_idx = associations[idx];
                    if (output_idx === null || output_idx === undefined)
                        continue;
                    insert.bind(output_idx + 1, row[idx]);
                }
                // Actually run the insert.
                insert.step();
                insert.reset(true);
            }
        };

        // Begin parsing and register the promise of completion. This is used to
        // ensure all the data has been completely imported before moving to the next
        // stage.
        thread.promise = new Promise((resolve, reject) => {
            papaparse.parse(file, {
                skipEmptyLines: true,
                error(err) {
                    if (buffered === null)
                        database.exec("ROLLBACK;");
                    reject(err);
                },
                chunk(results, parser) {
                    // Extract the first row as the headers.
                    // TODO: Make this configurable from the frontend.
                    if (headers === null)
                        headers = results.data.shift();
                    // Buffer up enough rows to preview the data.
                    if (associations === null) {
                        buffered.push(...results.data);
                        // If there are enough items buffered, then:
                        // - Pause the parser.
                        // - Enclose some state and register a callback associated
                        //   with the sequence ID.
                        // - Message the frontend asking for an association between
                        //   the input headers and the table columns.
                        if (buffered.length >= preview) {
                            parser.pause();
                            thread.resume = (a) => {
                                associations = a;
                                database.exec("BEGIN;");
                                process_rows(buffered);
                                buffered = null;
                                parser.resume();
                            };
                            postMessage({
                                tag: "associate",
                                seqid,
                                headers,
                                // Send exactly the amount requested to be previewed.
                                preview: buffered.slice(0, preview),
                            });
                        }
                        // Don't process any rows until after the input headers are
                        // associated.
                        return;
                    }
                    // Process the new rows.
                    process_rows(results.data);
                },
                complete() {
                    // `buffered` is `null` when importing has started.
                    if (buffered === null) {
                        database.exec("COMMIT;");
                        postMessage({ tag: "imported", seqid: seqid });
                        resolve();
                    } else {
                        // Register to import the buffer in its entirety upon resuming.
                        thread.resume = (a) => {
                            associations = a;
                            try {
                                database.exec("BEGIN;");
                                process_rows(buffered);
                                database.exec("COMMIT;");
                            } catch {
                                database.exec("ROLLBACK;");
                            }
                            buffered = null;
                            postMessage({ tag: "imported", seqid: seqid });
                            resolve();
                        };
                        postMessage({
                            tag: "associate",
                            seqid,
                            headers,
                            preview: buffered,
                        });
                    }
                },
            });
        });
    },
    // Receive the association between input headers and output headers that the user
    // defined.
    ["import"]: ({ seqid, associations }) => {
        import_threads.get(seqid).resume(associations);
    },
    // Receive a cancellation for one of the in-progress requests.
    cancel: ({ seqid, reason }) => {
        import_threads.get(seqid).promise.reject(reason);
        import_threads.delete(seqid);
    },
    // Notification from the frontend that no more inputs will be coming and that we
    // can start processing the data for download.
    process: async () => {
        const promises = [];
        for (const request of import_threads.values()) {
            promises.push(request.promise);
        }
        await Promise.all(promises);

        // Report to the frontend the highest rowid for progress display purposes.
        const [progress_max] = database.exec({
            sql: "SELECT max(rowid) FROM Data;",
            returnValue: "resultRows",
            rowMode: "array",
        });

        // Iteratively look for rows which have fuzzy matching suspected duplicates.
        // Notify the user and allow them to address it.
        const searches = fuzzy.map(() => ({
            highwater: null,
            exhausted: false,
        }));
        do {
            const lowest_search = searches.reduce(
                (acc, s) => s.highwater < acc ?
                    s.highwater :
                    acc,
                progress_max,
            );
            postMessage({
                tag: "progress",
                progress: lowest_search / progress_max,
                row: lowest_search,
                max: progress_max,
            });
            for (let idx = 0; idx < fuzzy.length; idx++) {
                const search = searches[idx];
                // Skip this column if it has already been exhausted.
                if (search.exhausted)
                    continue;
                const stmt = fuzzy[idx];
                // Bind the column's search highwater so the query can seek back to the
                // same position efficiently.
                stmt.bind(1, search.highwater);
                // Step the statement forward. If no result rows were produced, then mark
                // this column as exhausted and continue.
                if (!stmt.step()) {
                    search.exhausted = true;
                    stmt.reset(true);
                    continue;
                }
                // Get the rowid of the row which started the cluster. Save that as the
                // new highwater.
                search.highwater = stmt.getInt(0);
                // Extract the cluster information as JSON. The JSON is constructed
                // directly in the query itself from the agregate of all suspected
                // matches along this column.
                const cluster = stmt.getJSON(1);
                // Reset the statement before moving too far forward.
                stmt.reset(true);
                // Mark each of the rows as suspects.
                for (const row of cluster) {
                    suspect.bind(1, row.id);
                    suspect.step();
                    suspect.reset(true);
                }
                // Report the suspected duplicate cluster to the frontend for them to act
                // upon.
                postMessage({ tag: "review", id: search.highwater, cluster });
            }
        } while (!searches.every(s => s.exhausted));

        postMessage({ tag: "processed" });
    },
    export: () => {
        // Create a data URL from the more certain rows.
        const merged = URL.createObjectURL(exportCsv(export_data, "merged.csv"));

        // Create a data URL for the suspected rows.
        const review = fuzzy.length > 0
            ? URL.createObjectURL(exportCsv(export_suspects, "review.csv"))
            : null;

        // Send the download link to the frontend.
        postMessage({ tag: "exported", merged, review });
    },
    reviewed: ({ dropped }) => {
        // Drop every row which was determined a duplicate by the user.
        for (const id of dropped) {
            drop.bind(1, id);
            drop.step();
            drop.reset(true);
        }
    },
};

onmessage = (ev) => {
    const handler = handlers[ev.data.tag];
    delete ev.data.tag;
    handler(ev.data);
};

function exportCsv(stmt, name) {
    const escape = n => n.replace("\"", "\"\"");
    const lines = [stmt.getColumnNames().map(escape).join(",") + "\r\n"];
    // Repeatedly step through capturing each of the rows.
    while (stmt.step()) {
        lines.push(stmt.get([]).map(escape).join(",") + "\r\n");
    }
    stmt.reset(true);
    // Wrap the lines in a `File` including metadata.
    return new File(lines, name, {
        type: "text/csv; charset=utf-8",
    });
}
