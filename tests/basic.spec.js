import { test, expect } from "@playwright/test";

test("basic usage", async ({ page }) => {
  await page.goto("/");

  // Output headers.
  await page.getByLabel("Name").fill("email");
  await page.getByLabel("Required").check();
  await page.getByLabel("Unique").check();
  await page.getByLabel("Case Sensitive").uncheck();

  await page.getByRole("button", { name: "Add Header" }).click();
  await page.getByLabel("Name").nth(1).fill("first_name");

  await page.getByRole("button", { name: "Add Header" }).click();
  await page.getByLabel("Name").nth(2).fill("last_name");

  expect(await page.screenshot()).toMatchSnapshot("output-headers.png");
  await page.getByRole("button", { name: "Next" }).click();

  // Import files.
  await page.getByRole("button", { name: "Add File", disabled: false }).click();
  await page.getByLabel("Input File").focus();
  await page.getByLabel("Input File").setInputFiles("tests/left.csv");
  await page.getByLabel("Associate Header").nth(0).selectOption("email");
  await page.getByLabel("Associate Header").nth(1).selectOption("first_name");
  await page.getByLabel("Associate Header").nth(2).selectOption("last_name");
  await expectTableSnapshot(page.locator("table").nth(1), "import-1.json");
  await page.getByRole("button", { name: "Import" }).nth(0).click();

  await page.getByRole("button", { name: "Add File", disabled: false }).click();
  await page.getByLabel("Input File").nth(1).focus();
  await page.getByLabel("Input File").nth(1).setInputFiles("tests/right.csv");
  await page.getByLabel("Associate Header").nth(3).selectOption("first_name");
  await page.getByLabel("Associate Header").nth(4).selectOption("email");
  await page.getByLabel("Associate Header").nth(5).selectOption("last_name");
  await expectTableSnapshot(page.locator("table").nth(2), "import-2.json");
  await page.getByRole("button", { name: "Import" }).nth(1).click();

  await page.getByRole("button", { name: "Start", disabled: false }).scrollIntoViewIfNeeded()
  expect(await page.screenshot()).toMatchSnapshot("import-files.png");
  await page.getByRole("button", { name: "Start", disabled: false }).click();

  await page.getByRole("button", { name: "Export", disabled: false }).click();

  // Download output.
  const download_promise = page.waitForEvent("download");
  await page.getByRole("link", { name: "Merged" }).click();
  const download = await download_promise;
  const stream = await download.createReadStream();
  const buffers = [];
  for await (const data of stream) {
    buffers.push(data);
  }
  expect(Buffer.concat(buffers)).toMatchSnapshot("output.csv");
  expect(await page.screenshot()).toMatchSnapshot("after-download.png");
});

test("large files", async ({ page }) => {
  await page.goto("/");

  // Output headers.
  await page.getByLabel("Name").fill("email");
  await page.getByLabel("Required").check();
  await page.getByLabel("Unique").check();
  await page.getByLabel("Case Sensitive").uncheck();

  await page.getByRole("button", { name: "Add Header" }).click();
  await page.getByLabel("Name").nth(1).fill("first_name");

  await page.getByRole("button", { name: "Add Header" }).click();
  await page.getByLabel("Name").nth(2).fill("last_name");

  expect(await page.screenshot()).toMatchSnapshot("output-headers.png");
  await page.getByRole("button", { name: "Next" }).click();

  // Import files.
  await page.getByRole("button", { name: "Add File", disabled: false }).click();
  await page.getByLabel("Input File").focus();
  await page.getByLabel("Input File").setInputFiles("tests/large-1.csv");
  await page.getByLabel("Associate Header").nth(14).selectOption("email");
  await page.getByLabel("Associate Header").nth(2).selectOption("first_name");
  await page.getByLabel("Associate Header").nth(3).selectOption("last_name");
  await expectTableSnapshot(page.locator("table").nth(1), "large-import-1.json");
  await page.getByRole("button", { name: "Import" }).nth(0).click();

  await page.getByRole("button", { name: "Add File", disabled: false }).click();
  await page.getByLabel("Input File").nth(1).focus();
  await page.getByLabel("Input File").nth(1).setInputFiles("tests/large-2.csv");
  await page.getByLabel("Associate Header").nth(33 + 14).selectOption("email");
  await page.getByLabel("Associate Header").nth(33 + 2).selectOption("first_name");
  await page.getByLabel("Associate Header").nth(33 + 3).selectOption("last_name");
  await expectTableSnapshot(page.locator("table").nth(2), "large-import-2.json");
  await page.getByRole("button", { name: "Import" }).nth(1).click();

  await page.getByRole("button", { name: "Start", disabled: false }).scrollIntoViewIfNeeded()
  expect(await page.screenshot()).toMatchSnapshot("large-import-files.png");
  await page.getByRole("button", { name: "Start", disabled: false }).click();

  await page.getByRole("button", { name: "Export", disabled: false }).click();

  // Download output.
  const download_promise = page.waitForEvent("download");
  await page.getByRole("link", { name: "Merged" }).click();
  const download = await download_promise;
  const stream = await download.createReadStream();
  const buffers = [];
  for await (const data of stream) {
    buffers.push(data);
  }
  expect(Buffer.concat(buffers)).toMatchSnapshot("large-output.csv");
  expect(await page.screenshot()).toMatchSnapshot("large-after-download.png");
});

async function expectTableSnapshot(table, name) {
  const rows = [];
  for (const tr of await table.locator("tr").all()) {
    const row = await tr.locator("td").allInnerTexts();
    if (row.length == 0)
      continue;
    rows.push(row);
  }
  expect(JSON.stringify(rows, null, 2)).toMatchSnapshot(name);
}